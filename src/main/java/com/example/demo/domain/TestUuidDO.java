package com.example.demo.domain;

import javax.persistence.Entity;
import java.util.UUID;

@Entity
public class TestUuidDO extends BaseDO<UUID> {

    private String name;

    public static TestUuidDO create(String name) {
        TestUuidDO test = new TestUuidDO();
        test.setName(name);
        return test;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
