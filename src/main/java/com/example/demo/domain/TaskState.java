package com.example.demo.domain;

public enum TaskState {

    PENDING,
    FINISHED;

}
