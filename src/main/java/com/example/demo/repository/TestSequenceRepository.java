package com.example.demo.repository;

import com.example.demo.domain.TestSequenceDO;
import org.springframework.data.repository.CrudRepository;

public interface TestSequenceRepository extends CrudRepository<TestSequenceDO, Long> {

}
