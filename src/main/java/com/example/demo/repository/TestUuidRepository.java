package com.example.demo.repository;

import com.example.demo.domain.TestUuidDO;
import org.springframework.data.repository.CrudRepository;

import java.util.UUID;

public interface TestUuidRepository extends CrudRepository<TestUuidDO, UUID> {

}
