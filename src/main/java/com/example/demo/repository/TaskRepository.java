package com.example.demo.repository;

import com.example.demo.domain.TaskDO;
import org.springframework.data.repository.CrudRepository;

public interface TaskRepository extends CrudRepository<TaskDO, Long> {

}
