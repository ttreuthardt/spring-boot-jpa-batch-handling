package com.example.demo.controller;

import com.example.demo.domain.TestSequenceDO;
import com.example.demo.domain.TestUuidDO;
import com.example.demo.repository.TestSequenceRepository;
import com.example.demo.repository.TestUuidRepository;
import com.example.demo.service.TaskProcessor;
import com.example.demo.service.TestService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StopWatch;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.websocket.server.PathParam;
import java.util.UUID;

@RestController
@RequestMapping("/")
public class TestController {

    private static Logger logger = LoggerFactory.getLogger(TestController.class);

    private final TestService testService;
    private final TaskProcessor taskProcessor;
    private final TestSequenceRepository testSequenceRepository;
    private final TestUuidRepository testUuidRepository;

    public TestController(TestService testService, TaskProcessor taskProcessor,
                          TestSequenceRepository testSequenceRepository,
                          TestUuidRepository testUuidRepository) {
        this.testService = testService;
        this.taskProcessor = taskProcessor;
        this.testSequenceRepository = testSequenceRepository;
        this.testUuidRepository = testUuidRepository;
    }

    @GetMapping("seq")
    public Iterable<TestSequenceDO> getAllSequence() {
        return testSequenceRepository.findAll();
    }


    @GetMapping("uuid")
    public Iterable<TestUuidDO> getAllUuid() {
        return testUuidRepository.findAll();
    }


    @GetMapping("uuid/{uuid}")
    public ResponseEntity<TestUuidDO> findAllUuid(@PathVariable("uuid") UUID uuid) {
        return ResponseEntity.of(testUuidRepository.findById(uuid));
    }

    @DeleteMapping("uuid/{uuid}")
    public void deleteUuid(@PathVariable(name = "uuid") UUID uuid) {
        testUuidRepository.deleteById(uuid);
    }


    @GetMapping("insertSeq")
    public long insertTestSequenceData() {
        return runTimed(testService::insertTestSequenceData);
    }


    @GetMapping("insertUuid")
    public long insertTestUuidData() {
        return runTimed(testService::insertTestUuidData);
    }

    @GetMapping("insertTasks")
    public long insertTaskData() {
        return runTimed(testService::insertTaskData);
    }

    @GetMapping("process")
    public long process() {
        return runTimed(() -> taskProcessor.processChunk(100));
    }

    private long runTimed(Runnable runnable) {
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();

        runnable.run();

        stopWatch.stop();
        long lastTaskTimeMillis = stopWatch.getLastTaskTimeMillis();
        logger.info("Data inserted in {}ms", lastTaskTimeMillis);
        return lastTaskTimeMillis;
    }

}
