package com.example.demo.service;

import com.example.demo.domain.TaskDO;
import com.example.demo.domain.TaskState;
import org.hibernate.LockOptions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.LockModeType;
import java.util.List;

import static java.util.stream.Collectors.toList;

@Service
public class TaskProcessor {

    private static Logger logger = LoggerFactory.getLogger(TaskProcessor.class);

    private final EntityManager entityManager;

    public TaskProcessor(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Transactional
    public int processChunk(int number) {
        List<TaskDO> tasks = entityManager.createQuery(
                "select t " +
                        "from TaskDO t " +
                        "where t.state = :state " +
                        "order by t.id", TaskDO.class)
                .setParameter("state", TaskState.PENDING)
                .setMaxResults(10)
                .setLockMode(LockModeType.PESSIMISTIC_WRITE)
                .setHint("javax.persistence.lock.timeout", LockOptions.SKIP_LOCKED)
                .getResultList();

        int records = tasks.size();
        logger.info("{} Processing {} tasks, ids {}", number, records, tasks.stream().map(TaskDO::getId).collect(toList()));

        if (number == 1 && records > 0) {
            logger.info("######################## sleep");
            try {
                Thread.sleep(5_000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            logger.info("######################## done sleeping");
        }

        tasks.forEach(taskDO -> taskDO.setState(TaskState.FINISHED));

        return records;
    }

}
