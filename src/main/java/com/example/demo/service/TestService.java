package com.example.demo.service;

import com.example.demo.domain.TaskDO;
import com.example.demo.domain.TestSequenceDO;
import com.example.demo.domain.TestUuidDO;
import com.example.demo.repository.TaskRepository;
import com.example.demo.repository.TestSequenceRepository;
import com.example.demo.repository.TestUuidRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

@Service
public class TestService {

    private static Logger logger = LoggerFactory.getLogger(TestService.class);

    private final TestSequenceRepository testSequenceRepository;
    private final TestUuidRepository testUuidRepository;
    private final TaskRepository taskRepository;

    public TestService(TestSequenceRepository testSequenceRepository,
                       TestUuidRepository testUuidRepository, TaskRepository taskRepository) {
        this.testSequenceRepository = testSequenceRepository;
        this.testUuidRepository = testUuidRepository;
        this.taskRepository = taskRepository;
    }

    @Transactional
    public void insertTestSequenceData() {
        List<TestSequenceDO> tests = new ArrayList<>(10000);
        IntStream.range(0, 10000).forEach(value -> tests.add(TestSequenceDO.create("name-" + value)));

        testSequenceRepository.saveAll(tests);
    }

    @Transactional
    public void insertTestUuidData() {
        List<TestUuidDO> tests = new ArrayList<>(10000);
        IntStream.range(0, 10000).forEach(value -> tests.add(TestUuidDO.create("name-" + value)));

        testUuidRepository.saveAll(tests);
    }

    @Transactional
    public void insertTaskData() {
        List<TaskDO> tasks = new ArrayList<>(10000);
        IntStream.range(0, 10000).forEach(value -> tasks.add(TaskDO.create("task-" + value)));

        taskRepository.saveAll(tasks);
    }

}
