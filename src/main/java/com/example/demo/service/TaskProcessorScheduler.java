package com.example.demo.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.atomic.AtomicInteger;

import static java.util.concurrent.TimeUnit.MILLISECONDS;

//@Service
public class TaskProcessorScheduler {

    public static final int TASK_PROCESSOR_COUNT = 2;
    private static Logger logger = LoggerFactory.getLogger(TaskProcessorScheduler.class);

    private ScheduledExecutorService executorService = new ScheduledThreadPoolExecutor(4);

    private final TaskProcessor taskProcessor;
    private final AtomicInteger number = new AtomicInteger();

    public TaskProcessorScheduler(TaskProcessor taskProcessor) {
        this.taskProcessor = taskProcessor;
    }

    @PostConstruct
    public void scheduleTaskProcessors() {
        logger.info("scheduleTaskProcessors");
        for (int i = 0; i < TASK_PROCESSOR_COUNT; i++) {
            final int number = this.number.incrementAndGet();
            executorService.scheduleWithFixedDelay(() -> process(number), 1_000, 2_000, MILLISECONDS);
        }
    }

    private void process(int i) {
        int records = 0;
        do {
            records = taskProcessor.processChunk(i);
        } while (records > 0);
    }

    @PreDestroy
    public void shutdownTaskProcessors() {
        logger.info("shutdownTaskProcessors");
        executorService.shutdownNow();
    }
}
